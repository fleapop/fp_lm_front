module.exports = {
  devServer: {
    // proxy: {
    //   '/api': {
    //     // target: 'http://localhost:8081',
    //     changeOrigin: true
    //   }
    // },
    headers: { "Access-Control-Allow-Origin": "*" },
    // public: 'web.fleapop.co.kr:8080',
    disableHostCheck: true,
    compress: true
  },
  configureWebpack: {
    output: {
      libraryExport: 'default'
    }
  }
}