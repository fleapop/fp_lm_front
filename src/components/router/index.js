import store from './store-router'
import common from './common-router'

export default {
  name: "index",
  mixins: [
    store, common
  ]
}