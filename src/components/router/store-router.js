const None = () => import(/* webpackChunkName: "Store" */'../../views/_None')
const GoodsDetail = () => import(/* webpackChunkName: "Store" */'../../views/Store/GoodsDetail')
const GoodsInquiry = () => import(/* webpackChunkName: "Store" */'../../views/Store/GoodsInquiry')
const GoodsOrder = () => import(/* webpackChunkName: "Store" */'../../views/Store/GoodsOrder')
const GoodsOrderResult = () => import(/* webpackChunkName: "Store" */'../../views/Store/GoodsOrderResult')


export default {
  data () {
    return {
      componentList: {GoodsDetail, GoodsInquiry, None, GoodsOrder, GoodsOrderResult}
    }
  }
}