import Vue from 'vue'
import App from './App.vue'
import router from './router'

import './assets/scss/lmTheme.scss'
import './assets/css/style.css'
import './assets/scss/style.scss'

import Plugins from './lib/plugins'

import subVueMixin from  './mixins/subview-stack-mixin'
import store from './store'
import DynamicComponentInject from '@/lib/dynamicComponentInject'
import LmPopupLayer from "@/components/LmPopupLayer";

import './lib/vue-prototypes'
import './lib/vue-filters'


// import IMP from 'vue-iamport'
// Vue.use(IMP, 'imp56251768')
// Vue.IMP().load()

let IMP = window.IMP; // 생략해도 괜찮습니다.
IMP.init("imp56251768");
// IMP.init("imp09395281");

Vue.component('lm-popup-layer', LmPopupLayer)

Vue.config.productionTip = false
// Vue.http.options.crossOrigin = true
Vue.mixin(subVueMixin)
Vue.use(Plugins)

import VueCryptojs from 'vue-cryptojs'
Vue.use(VueCryptojs)

import VScrollLock from 'v-scroll-lock'
Vue.use(VScrollLock)

new Vue({
  router,
  store,
  created () {
    Vue.use(DynamicComponentInject, {store: store, router: router})
  },
  render: h => h(App),
}).$mount('#app')
