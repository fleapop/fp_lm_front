// import Vue from 'vue'
// import Storage from '@/utils/storage'
// import _ from 'lodash'
// import http from '@/apis/http'
// import booking from '@/apis/booking'        // booking API

const state = {
  userData: {
    user_id: ''
  }
}

const mutations = {
  setUserData (state, params) {
    if (params) {
      const jsonStr = JSON.stringify(params)
      state.userData = JSON.parse(jsonStr)
      localStorage.setItem('user_data', jsonStr)
    }
  }
}
const actions = {
  setUserData ({ commit }, params) { commit('setUserData', params) }
}

const getters = {
  userData: state => state.userData,
}


export default {
  state,
  mutations,
  actions,
  getters
}
