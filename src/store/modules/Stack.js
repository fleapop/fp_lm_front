const state = {
  stackHistory: []
}

const mutations = {
  setStackHistory (state, params) {
    const jsonStr = JSON.stringify(params)
    state.stackHistory = JSON.parse(jsonStr)
    localStorage.setItem('stackHistory', jsonStr)
  }
}

const actions = {
  setStackHistory ({ commit }, params) {
    commit('setStackHistory', params)
  }
}

const getters = {
  getStackHistory: state => state.stackHistory
}

export default {
  state,
  mutations,
  actions,
  getters
}
