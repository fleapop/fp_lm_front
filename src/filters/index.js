import utils from '../lib/'
/**
 * @author dhlee0@smartscore.kr
 * @version 0.1
 * @since 2020년 01월 29일
 * @description 유틸리티 함수를 자동으로 필터로 등록한다.
 수정 로그
 version 0.1
 */
const noInstall = [] // filter 로 자동으로 등록되면 안되는 유틸리티 등록 문자열로 등록한다.
const flatObject = obj => {
  return Object.keys(obj).reduce((p, c) => {
    if (noInstall.indexOf(c) !== -1) return p
    if (typeof obj[c] === 'function') p[c] = obj[c]
    if (typeof obj[c] === 'object') Object.assign(p, flatObject(obj[c]))
    return p
  }, {})
}
// const fn = flatObject(utils)
// console.log('filter::', fn)
// export default {
//   install (Vue) {
//     console.log('filters::utils::', uitls, Object.keys(uitls))
//     // Object.keys(uitls).filter(name => noInstall.indexOf(name) !== -1).map(name => Vue.filter(name, uitls[name]))
//   }
// }
// 문자열 관련 처리 및 날짜 처리만 필터로 등록
export default flatObject({...utils.format, ...utils.date})
