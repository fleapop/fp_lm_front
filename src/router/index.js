import Vue from 'vue'
import Router from 'vue-router'

import routes from './routing'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: '/',
  routes: routes
  // scrollBehavior (to, from, savedPosition) {
  //   console.log(savedPosition)
  //   if (savedPosition) {
  //     return savedPosition
  //   } else {
  //     return { x: 0, y: 0 }
  //   }
  // }
})
