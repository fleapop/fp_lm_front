export default {
  path: '/Terms',
  name: 'Terms',
  children: [
    {
      path: 'AccessTerms',
      name: 'AccessTerms',
      component: () => import(/* webpackChunkName: "terms" */'../../views/Terms/AccessTerms')
    },
    {
      path: 'PersonalData',
      name: 'PersonalData',
      component: () => import(/* webpackChunkName: "terms" */'../../views/Terms/PersonalData')
    }
  ]
}