export default {
  path: '/',
  name: 'Home',
  redirect: 'HomeStore',
  props: true,
  component: () => import(/* webpackChunkName: "Home" */'../../views/Home'),
      children: [
        {
            path: 'Store/:homeTab?/:comp?/:param?',
            name: 'HomeStore',
            props: true,
            component: () => import(/* webpackChunkName: "Home" */'../../views/Home/Store')
        },
        {
            path: 'Magazine',
            name: 'HomeMagazine',
            props: true,
            component: () => import(/* webpackChunkName: "Home" */'../../views/Home/Magazine')
        },
        {
          path: 'Offline',
          name: 'HomeOffline',
          props: true,
          component: () => import(/* webpackChunkName: "Home" */'../../views/Home/LMarket')
        },
        {
          path: 'MyPage',
          name: 'HomeMyPage',
          props: true,
          component: () => import(/* webpackChunkName: "Home" */'../../views/Home/MyPage')
        }
    ]
}