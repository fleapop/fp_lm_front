/**
 * The file enables '@/router/index.js' to import all routers
 * in a one-shot manner. There should not be any reason to edit this file.
 */

// import Intro from '../../Intro'

/**
 * Default Routing, Intro
 * @type {{path: string, name: string, component}}
 */
const root = {
    path: '/',
    name: 'Intro',
    redirect: 'Store/Store',
    component: () => import(/* webpackChunkName: "intro" */'../../App')
}
/**
 * Default Routing, Blank Page
 * @type {{path: string, name: string}}
 */
// const blank = {
//     path: '/Blank',
//     name: 'Blank'
// }

/**
 * Default Not Found Page
 * @type {{path: string, name: string, component: function()}}
 */
// const NotFound = {
//     path: '/NotFound',
//     name: 'NotFound',
//     component: () => import(/* webpackChunkName: "intro" */'../../NotFound')
// }
//
// const NotLogin = {
//     path: '/NotLogin',
//     name: 'NotLogin',
//     component: () => import(/* webpackChunkName: "intro" */'../../NotLogin')
// }
//
// const ThemeComponents = {
//     path: '/Theme',
//     name: 'ThemeComponents',
//     component: () => import(/* webpackChunkName: "intro" */'../../components/ThemeComponents')
// }
//
// const Components = {
//     path: '/Components',
//     name: 'ComponentsSample',
//     component: () => import(/* webpackChunkName: "intro" */'../../components/Components')
// }

/**
 * Default Routing, Not Found to Redirect Root
 * @type {{path: string, name: string, redirect: string}}
 */
// const all = {
//     path: '*',
//     name: 'NotFound',
//     component: () => import(/* webpackChunkName: "intro" */'../../NotFound')
// }

const files = require.context('.', false, /\.js$/)
const routings = [root];

// const routings = [root]

files.keys().forEach(key => {
    if (key === './index.js') return
    // if (process.env.NODE_ENV === 'production' && key === './layouts.js') return
    routings.push(files(key).default)
})

export default routings
