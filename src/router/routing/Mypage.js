export default {
  path: '/Mypage',
  props: true,
  component: () => import(/* webpackChunkName: "store" */'../../views/MyPage'),
  children: [
      {
          path: 'Login',
          name: 'userLogin',
          props: true,
          component: () => import(/* webpackChunkName: "store" */'../../views/MyPage/Login')
      }
  ]
}