export default {
  path: '/Layout',
  props: true,
  component: () => import(/* webpackChunkName: "store" */'../../views/Layout'),
  children: [
    {
      path: 'Components',
      name: 'LayoutComponents',
      props: true,
      component: () => import(/* webpackChunkName: "store" */'../../views/Layout/Component')
    }
  ]
}