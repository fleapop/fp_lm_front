export default {
  path: '/Common/:comp?/:param?',
  name: 'Common',
  props: true,
  component: () => import(/* webpackChunkName: "common" */'../../views/Common'),
}