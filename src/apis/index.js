import http from './http'

const files = require.context('.', false, /\.js$/)
const apis = files.keys().filter(key => !(key.endsWith('index.js')) && !(key.endsWith('http.js'))).reduce((p, key) => {
  p[key.replace(/.\/|.js/gi, '')] = (files(key).default)(http)
  return p
}, {})
console.log('apis::modules::', apis)
export default { http, apis }
