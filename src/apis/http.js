
import '@/utils'
import Native from '@/lib/native'
import axios from 'axios'
import Vue from 'vue'

const qs = require('querystring')

let netObj = {
  showError: false, // 에러팝업이 오픈됐는지 여부

  firstTimer: null, // 첫 리퀘스트가 3초이상 걸리면 프로그래스 출력
  slowNetLimit: 3000, // 설정 시간 이상의 시간이 걸린 리퀘스트가 있으면 느린 useProgress가 셋된다
  useProgress: false, // 프로그래스를 오픈할지 여부 (이전 요청이 3초이상 걸렸으면 셋됨)
  showProgress: false, // 프로그래스가 오픈됐는지 여부
  openTimer: null, // 프로그래스를 오픈할때 1초 후에 오픈하도록 처리
  closeTimer: null, // 프로그래스가 오픈한지 1초도 안되서 닫히게 될경우 1초후에 닫도록 처리
  openedTime: 0, // 프로그래스 오픈된 시간
  resetError () {
    this.showError = false
    this.errorQueue = []
  },
  addRequest (o, type = 'req') {
    if (this.requestQueue.length === 0) {
      this._log('reservProgress')
      if (this.firstTimer) clearTimeout(this.firstTimer)
      this.firstTimer = setTimeout(_ => {
        this._log('firstProgress')
        if (this.requestQueue.length !== 0 || this.retryQueue.length !== 0) {
          this.useProgress = true
          this.openProgress(o)
        }
      }, this.slowNetLimit)
    }
    if (type === 'req') this.requestQueue.push(o)
    else if (type === 'retry') this.retryQueue.push(o)
  },
  cancel () {
    if (this.requestQueue.length > 0) {
      this.requestQueue.forEach(o => { o.cancel = true })
      this.requestQueue = []
    }
    if (this.retryQueue.length > 0) {
      this.retryQueue.forEach(o => { o.cancel = true })
      this.errorQueue = []
    }
  },
  remove (o) {
    let idx = this.requestQueue.indexOf(o)
    if (idx !== -1) this.requestQueue.splice(idx, 1)
    idx = this.errorQueue.indexOf(o)
    if (idx !== -1) this.errorQueue.splice(idx, 1)
    idx = this.retryQueue.indexOf(o)
    if (idx !== -1) this.retryQueue.splice(idx, 1)
  },
  openProgress (o) {
    this._log('openProgress')
    if (this.useProgress && !o.noProgress && Vue.prototype.$progressLayer) {
      if (!this.openTimer) {
        this._log('setTimerProgress')
        this.openTimer = setTimeout(_ => {
          this._log('--opendProgress')
          Vue.prototype.$progressLayer.open()
          this.openTimer = null
          this.openedTime = new Date().getTime()
          this.showProgress = true
          if (this.closeTimer) {
            clearTimeout(this.closeTimer)
            this.closeTimer = null
          }
        }, 1000)
      } else {
        if (this.closeTimer) {
          clearTimeout(this.closeTimer)
          this.closeTimer = null
        }
      }
    }
  },
  closeProgress (o) {
    this.remove(o)
    this._log('closeProgress')

    if (this.firstTimer) {
      clearTimeout(this.firstTimer)
      this.firstTimer = null
    }

    if (this.openTimer) {
      clearTimeout(this.openTimer)
      this.openTimer = null
    }

    // check time
    this.useProgress = new Date().getTime() - o.start >= this.slowNetLimit
    if (this.openedTime === 0) return

    if (this.requestQueue.length === 0 && this.retryQueue.length === 0) {
      let passed = new Date().getTime() - this.openedTime
      console.log('passed', passed)
      if (passed < 1000) this.closeTimer = setTimeout(_ => { this._closePrg() }, 1000 - passed)
      else this._closePrg()
    }
  },
  _log (title) {
    console.log('#' + title, new Date().format('mm:ss'), 'use:' + this.useProgress, 'openedTime:' + this.openedTime, 'openTimer:' + this.openTimer, 'reqLen:' + this.requestQueue.length, 'retryLen:' + this.retryQueue.length)
  },
  _closePrg () {
    Vue.prototype.$progressLayer.close()
    this.showProgress = false
    this.openedTime = 0
  },
  requestQueue: [], // 최초 요청시
  errorQueue: [], // 에러 발생시
  retryQueue: [] // 에러 후 새로고침시
}

Vue.prototype.$netObj = netObj


let request = (o) => {
  let resolve = o.resolve
  let reject = o.reject
  let url = o.url
  let formData = o.formData
  let Options = o.Options
  let Method = o.Method

  o.start = new Date().getTime()
  Options.timeout = 30000
  Options.mode = 'cors'

  netObj.openProgress(o)
  axios.post(url, formData, Options).then(res => {
    netObj.closeProgress(o)

    if(res.data.result === undefined) {
      resolve(res.data)
    } else {
      if (res.data.result) {
        resolve(res.data)
      } else {
        reject(res.data.fail)
      }
    }
  }).catch(e => {
    reject(fail)
  })
}

// let domain = '127.0.0.1:8000'
let domain = 'lmapi.fleapop.co.kr'
   
// let protocol = 'http://'
let protocol = 'https://'

if (window.location.hostname.indexOf('test') >= 0) {
  domain = 'lmapitest.fleapop.co.kr'
  // protocol = 'https://'
}

// let domain = 'wbe.fleapop.co.kr:8081'
// let protocol = location.protocol + '//'

let http = {
  post (Method, Data = {}, Files = undefined, Options = {}, shouldFwdToJapis = false) {
    if (!Options.hasOwnProperty('headers')) Options['headers'] = {'Content-Type': 'application/x-www-form-urlencoded'}
    // let userData = this.$store.getters.userData
    // Data.user_id = userData.user_id
    // Data.token = userData.token
    // Data.device = 101

    let params = '';
    // let userData = this.$store.getters.userData
    let userData = JSON.parse(localStorage.getItem('user_data'))
    if (userData !== null && userData.user_idx !== '' && Method !== 'user/native/login') {
      Data.user_id = userData.user_id
      Data.token = userData.token
      Data.device = Native.getOsTypeCode()
    }

    let formData = qs.stringify(Data)
    if (Files !== undefined) {
      formData = new FormData()
      Object.keys(Data).forEach(key => {
        formData.append(key, Data[key])
      })

      Options.headers['Content-Type'] = 'multipart/form-data'
      // Options.headers['cookie'] = JSON.stringify(this.$store.getters.userData)

      for (let f of Files) {
        formData.append('uploadfile[]', f)
      }
    }

    let url = ''
    // let protocol = location.protocol + '//'
    url = protocol + domain + '/api/' + Method

    return new Promise((resolve, reject) => {
      let o = {url, formData, Options, Method, resolve, reject, cancel: false, start: 0}
      request(o)
    })
  },
  get (Method) {
    const api = axios.create({
      headers: {
        'Content-type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
      }
    })
    let Options = {}
    const token = document.head.querySelector('meta[name="csrf-token"]');

    let params = '';
    // let userData = this.$store.getters.userData
    let userDataLs = JSON.parse(localStorage.getItem('user_data'))
    if (userDataLs !== null && userDataLs.user_idx !== '') {
      let userKey = 'user_id=' + userDataLs.user_id + '&token=' + userDataLs.token + '&device=103';
      params = (Method.indexOf('?') >= 0 ? "&" : "?") + userKey
    }


    let url = ''

    url = protocol + domain + '/api/' + Method + params

    return new Promise((resolve, reject) => {
      api.get(url).then(res => {
        console.log(res)
        if (res.data.result) {
          resolve(res.data)
        } else {
          reject(res.data.fail)
        }
      }).catch(e => {
        reject(fail)
      })
    })
  }
}

export default http