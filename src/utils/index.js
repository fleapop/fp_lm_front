const files = require.context('.', false, /\.js$/)
const modules = files.keys().filter(key => !(key.endsWith('index.js'))).reduce((p, key) => {
  p[key.replace(/.\/|.js/gi, '')] = files(key).default
  return p
}, {})
export default modules
