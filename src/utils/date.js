/*eslint no-cond-assign: ["error", "always"]*/
import moment from 'moment'

Date.prototype.format = function (f) {
  if (!this.valueOf()) return ''

  const WeekName = ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일']
  let d = this
  let h = 0

  return f.replace(/(yyyy|yy|MM|M|dd|d|E|e|hh|mm|ss|a\/p)/gi, function ($1) {
    switch ($1) {
      case 'yyyy':
        return d.getFullYear()
      case 'yy':
        return (d.getFullYear() % 1000).zerofill(2)
      case 'MM':
        return (d.getMonth() + 1).zerofill(2)
      case 'M':
        return (d.getMonth() + 1)
      case 'dd':
        return d.getDate().zerofill(2)
      case 'd':
        return d.getDate()
      case 'E':
        return WeekName[d.getDay()]
      case 'e':
        return WeekName[d.getDay()].substr(0, 1)
      case 'HH':
        return d.getHours().zerofill(2)
      case 'hh':
        h = d.getHours() % 12
        return ((h) ? h : 12).zerofill(2)
      case 'mm':
        return d.getMinutes().zerofill(2)
      case 'ss':
        return d.getSeconds().zerofill(2)
      case 'a/p':
        return d.getHours() < 12 ? '오전' : '오후'
      default:
        return $1
    }
  })
}

// Date.prototype.toSimpleDateString = function () {
//   console.info(this)
//   return this
// let mon = this.getMonth() + 1
// let dt = this.getDate()
// return this.getFullYear() + '/' + mon + '/' + dt
// }

Date.prototype.addHours = function (h) {
  this.setTime(this.getTime() + (h * 60 * 60 * 1000))
  return this
}

Date.prototype.addDates = function (val) {
  this.setDate(this.getDate() + val)
  return this
}

Date.prototype.addMonths = function (val) {
  this.setMonth(this.getMonth() + val)
  return this
}
export default {
  date (value, format = 'YYYY.MM.DD') {
    return moment(value).format(format)
  },
  dateTimeMin (value) {
    return value ? moment(value, 'YYYYMMDDHHmm').format('YYYY.MM.DD HH:mm') : ''
  },
  currentTimestamp () {
    return Math.floor(new Date().getTime() / 1000)
  },
  /**
   *  $utils.date.getDateFromString(str, 'yyyy.MM.dd')
   *  $utils.date.getDateFromString(str).format('yyyy.MM.dd')
   * */
  getDateFromString (str, format = false) {
    let dt = str.replace('T', ' ').replace(/-/g, '/').replace(/\./g, '/')
    // console.log('from date string: ' + str + ', to: ' + dt)
    return format ? (new Date(dt)).format(format) : new Date(dt)
  },
  /**
   * 필터로 쓸때
   *  {{dateString | dateFromString}}
   *  {{dateString | dateFromString('yyyy.**.** a/p **:**')}}
   * */
  dateFromString (dateString, format = 'yyyy.MM.dd HH:mm') {
    // console.log('dateFromString::', dateString, format, new Date(dateString))
    return dateString ? (new Date(dateString.replace('T', ' ').replace(/-/g, '/').replace(/\./g, '/'))).format(format) : dateString
  }
}
