/**
 * @module ComponentAppendMixin
 * @author dhlee0@smartscore.kr
 * @version 0.1
 * @since 2020년 06월 02일
 * @description 모달 생성시 태그를 루트에 붙여서 쌓이게 하는 기능 믹스인
 <code><pre></pre></code>

 수정 로그
 version 0.1
 */
export default {
  // name: 'ComponentAppendMixin', // plugin 방식에서 컴포넌트 name을 인식을 못해서 주석처리함
  data () {
    return {
      isAiOpen: false,
      componentAppendMixinUuid: 0
    }
  },
  props: {
    // 플러그인으로 온것인지 체크
    isPlugin: {
      type: Boolean,
      default: false
    }
  },
  computed: {},
  watch: {
    isAiOpen (newVal) {
      console.log('isAiOpen::watch::', newVal)
      if (newVal) {
        this.appendComponent()
      } else {
        this.$nextTick(() => {
          this.removeComponent()
        })
      }
    }
  },
  created () {
    // console.log('component-append-mixin::created::', this.isPlugin)
  },
  mounted () {
    console.log('component-append-mixin::mounted::')
    if (this.isPlugin) this.appendComponent()
    // if (this.isPlugin) {
    //   this.$nextTick(_ => {
    //     this.appendComponent()
    //   })
    //   // setTimeout(_ => {
    //   //   this.appendComponent()
    //   // }, 100)
    // }
  },
  methods: {
    appendComponent () {
      this.componentAppendMixinUuid = Date.now()
      console.log('component-append-mixin::appendComponent::', this.$el, this.componentAppendMixinUuid)
      // document.getElementById('app').appendChild(this.$el) // app에다 붙였더니 Vue인식이 안되서 body에 붙이는걸로 변경
      document.body.appendChild(this.$el)
    },
    removeComponent () {
      console.log('component-append-mixin::removeComponent::', this.$el, this.componentAppendMixinUuid)
      // this.$el.remove()
      document.body.removeChild(this.$el)
    }
  },
  beforeDestroy () {
    console.log('component-append-mixin::beforeDestroy::')
    if (this.isPlugin) this.removeComponent()
  }
}
