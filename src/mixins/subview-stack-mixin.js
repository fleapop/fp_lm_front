/**
 * @module subview-stack-mixin
 * @author ansc
 * @version 0.1
 * @since 2020년 10월 07일
 * @vue-data {String} uuid - 컴포넌트 uuid 값 query.uuid 값과 동일
 * @vue-data {String} name - 컴포넌트 이름
 * @vue-data {String | Number} param - 라우터에서 받는 param
 * @vue-data {Object} query - 라우터에서 받는 query 객체
 * @vue-data {Object} backup - 컴포넌트가 사라지고 다시 복구될때 기존 데이터 백업하는 용도
 * @vue-data {Object} option - 컴포넌트 제어 관련된 옵션이 들어간다.
 * @description subview-mixin 은 각 컴포넌트에서 사용할 인터페이스로 구현체가 존재하면 안된다.
 <code><pre>
 </pre></code>

 수정 로그
 version 0.1
 */

export default {
  data () {
    return {}
  },
  props: {
    uuid: {
      type: Number | String,
      default: 0
    },
    name: {
      type: String,
      default: ''
    },
    param: {
      type: Number | String,
      default: null
    },
    query: {
      type: Object,
      default: null
    },
    backup: {
      type: Object,
      default: null
    },
    option: {
      type: Object,
      default: null
    }
  },
  computed: {},
  watch: {},
  created () {
    if (this.backup) {
      this.melt(this.backup)
    }
  },
  mounted () {},
  filters: {},
  methods: {
    startComponent () {
      console.error('subview-stack-mixin Overloading to use startComponent')
    },
    resultComponent (obj) {
      console.error('subview-stack-mixin Overloading to use resultComponent::', obj)
    },
    updateComponent () {
      console.error('subview-stack-mixin Overloading to use updateComponent')
    },
    pausedComponent () {
      console.info('subview-stack-mixin Overloading to use pausedComponent')
    },
    /**
     * @author ansc
     * @version 0.1
     * @since 2020년 10월 07일
     * @param {Stirng} name - 호출할 컴포넌트 이름
     * @param {Object} query - $router 의 query 데이터
     * @param {String | Number} param - 라우터에 설정된 param, 주로 idx 값이 넘어오거나 없다.
     * @param {Object} option - 컴포넌트 제어하는데 필요한 속성 정의, {animation: 'slide-right'} 값이 기본
     수정 로그
     version 0.1

     stackPush : $router.push 동작
     stackReplace : $router.replace 동작
     stackBack : history.back 동작 전에 데이터를 전달하는 용도
     stackBackAll : history 의 기록을 전부 삭제하는 용도, 현재는 구현되 있지 않음
     */
    stackSubPush (comp, param = null, option = null, query = {}) {
      console.log('ComponentStack::stackPush::', comp, param, option, query)
      if (!query) query = {}
      // 기존에 uid 넘어와도 리셋하게한다.
      let tUuid = this.$querystring('uuid')
      query.uuid = 1
      if (tUuid) {
        query.uuid = parseInt(tUuid) + 1
      }
      // if (!info.fromUid) info.fromUid = this.stack[this.curIdx].uid
      let params = this.$route.params
      params.comp = comp
      params.param = param
      params.option = option

      this.$router.push({name: this.$route.name, params: params, query: query})
    },
    stackPush (name, query, param = null, option = {animation: 'page-slide-right'}) {
      // console.log('subview-stack-mixin stack::push::emit::', name, param, option, query)
      this.$emit('stackPush', name, param, option, query)
    },
    stackReplace (name, query, param = null, option = {animation: 'page-slide-right'}) {
      console.log('subview-stack-mixin stack::replace::emit::', name, param, option, query)
      this.$emit('stackReplace', name, param, option, query)
    },
    stackBack (name, data = false) {
      console.log('subview-stack-mixin stack::back::emit::', name, data)
      this.$emit('stackBack', name, this.uuid, data)
    },
    stackPass (name, data = false) {
      console.log('subview-stack-mixin stack::pass::emit::', name, data)
      this.$emit('stackPass', name, this.uuid, data)
    },
    stackBackAll () {
      console.log('subview-stack-mixin stack::backall::emit')
      this.$emit('stackBackAll')
    },
    getStackNames (callback) {
      console.log('subview-stack-mixin stack::getStackNames', this.$parent)
      this.$emit('getStackNames', callback)
    },

    /**
     * @param {Element} el - 애니메이션 afterEnter 훅 호출되는 엘리먼트
     * @description afterEnter 애니메이션 훅
     *
     수정 로그
     version 0.1
     */
    afterEnter (el) {
      console.log('subview-stack-mixin::afterEnter::event')
      this.$emit('afterEnter', el)
    },
    /**
     * @description 컴포넌트 적재가 특정갯수(7) 이상 넘어갈때 해당 컴포넌트를 삭제하는데 그 전에 data 를 백업한다.
     *
     수정 로그
     version 0.1
     */
    freeze () {
      console.log('------------------- freezing')
      let back = {}
      for (let k in this.$data) {
        console.log('---------------------- freeze', k, this[k])
        back[k] = this[k]
      }
      back.uuid = this.uuid
      back._ani = this.option.animation
      this.$emit('freeze', back)
    },
    /**
     * @param {Object} back
     * @description 컴포넌트 적재 하층이 다시 복구될 때 백업되어 있는 데이터로 복구한다.
     *
     수정 로그
     version 0.1
     */
    melt (back) {
      console.log('------------------- melting')
      for (let k in back) {
        console.log('---------------------- melt', k, back[k])
        if (this.hasOwnProperty(k)) this[k] = back[k]
      }
      this.$emit('freeze', {uuid: this.uuid})
    }
  }
}
