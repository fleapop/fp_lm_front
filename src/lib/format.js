
String.prototype.padLeft = function (len, char) {
  char !== undefined || (char = ' ')

  let s = ''
  let i = 0
  while (i++ < (len - this.length)) {
    s += char
  }
  return s + this
}

String.prototype.zerofill = function (len) {
  return this.padLeft(len, '0')
}

Number.prototype.zerofill = function (len) {
  return this.toString().zerofill(len)
}

Number.prototype.toRad = function () {
  return this * Math.PI / 180
}

export default {
  enc (str, key = '3dxbWFjbQ') {
    if (typeof (str) !== 'string') str = str.toString()
    let bytes = this.toBytes(str)
    let len = bytes.length
    let keyLen = key.length

    if (!len || !keyLen || keyLen < 3) return ''

    let ret = ''
    for (let i = 0; i < len; i++) {
      let keyCharAt = i % keyLen
      let keyChar = key[keyCharAt]
      let keyCode = keyChar.charCodeAt(0)

      let charCode = bytes[i]
      ret += String.fromCharCode(charCode ^ keyCode)
    }
    return btoa(ret)
  },
  toBytes (str) {
    let out = []
    let p = 0
    for (let i = 0; i < str.length; i++) {
      let c = str.charCodeAt(i)
      if (c < 128) {
        out[p++] = c
      } else if (c < 2048) {
        out[p++] = (c >> 6) | 192
        out[p++] = (c & 63) | 128
      } else if (((c & 0xFC00) === 0xD800) && (i + 1) < str.length && ((str.charCodeAt(i + 1) & 0xFC00) === 0xDC00)) {
        c = 0x10000 + ((c & 0x03FF) << 10) + (str.charCodeAt(++i) & 0x03FF)
        out[p++] = (c >> 18) | 240
        out[p++] = ((c >> 12) & 63) | 128
        out[p++] = ((c >> 6) & 63) | 128
        out[p++] = (c & 63) | 128
      } else {
        out[p++] = (c >> 12) | 224
        out[p++] = ((c >> 6) & 63) | 128
        out[p++] = (c & 63) | 128
      }
    }
    return out
  },
  str2Number (value) {
    return parseInt(value)
  },
  /**
   * @author dhlee0@smartscore.kr
   * @version 0.1
   * @since 2019년 09월 19일
   * @description 전화번호를 000-000-0000, 000-0000-0000 으로 자동 처리 한다.
   수정 로그
   version 0.1
   */
  mobileNumber (phone, mask = false) {
    // return str ? str.replace(/-/gi, '').replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/, '$1-$2-$3') : ''
    return phone ? phone.replace(/-/gi, '').replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/, mask ? '$1-****-$3' : '$1-$2-$3') : phone
  },
  /**
   * @author dhlee0@smartscore.kr
   * @version 0.1
   * @since 2019년 12월 27일
   * @param {Number} phone - 전화번호
   * @description 전화번호가 숫자로 오면 000-000-0000, 000-0000-0000 형태로 변환해서 리턴
   수정 로그
   version 0.1
   */
  // mobileNumberConveter (phone) {
  //   return phone ? phone.replace(/-/gi, '').replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/, '$1-$2-$3') : phone
  // },
  /**
   * @author dhlee0@smartscore.kr
   * @version 0.1
   * @since 2019년 12월 27일
   * @param {Number} str - 이모지가 없어야 하는 텍스트
   * @description 이모지를 제외한 모든 텍스트 통과, 천지인도 통과
   수정 로그
   version 0.1
   deleteEmoji 디렉티브도 있음 같은 기능임 나중에 통합 해야함
   */
  deleteEmojiConveter (str) {
    return (str) ? str.toString().replace(/[^0-9a-zA-Z\{\}\[\]\/?.,;:|\)*~`!^\-+<>@\#$%&\\\=\(\'\"ㄱ-ㅎ|ㅏ-ㅣ|가-힣|\u318D\u119E\u11A2\u2022\u2025\u00B7\uFE55\u4E10\s+]/gim, '') : str
  },
  // Comma3 랑 동일한 코드 Comma3 삭제 요망
  numberComma (value) {
    let reg = /(^[+-]?\d+)(\d{3})/
    value += ''
    value = value.replace(/,/gi, '')
    while (reg.test(value)) {
      value = value.replace(reg, '$1' + ',' + '$2')
    }
    return value
  },
  uppercase (input) {
    return input.toUpperCase()
  },
  first4Chars (str) { return str.substring(0, 4) },
  last4Chars (str) { return str.substring(str.length - 4) },
  shorAddr (addrStr, splitStr = ' ') {
    const addrs = addrStr.split(' ')
    if (addrs.length < 2) {
      return ''
    }
    const [city, gu] = addrs
    return `${city}${splitStr}${gu}`
  },
  floatFixed (num, count) {
    return num.toFixed(count)
  },
  zeroNullToSymbol (value, symbol = '-') {
    if (value === undefined || value === null || value === 0) return symbol
    return parseFloat(value).toFixed(1)
  },
  /**
   * @author dhlee0@smartscore.kr
   * @version 0.1
   * @since 2020년 05월 14일
   * @description json 파싱할때 개행문자 에러나서 만든 함수
   수정 로그
   version 0.1
   */
  stringEnterCharConvert (str) {
    return str.replace(/\n/gi, '\\n').replace(/\r/gi, '\\r')
  },
  checkStrLenOver (str, korLimit, engLimit) {
    let asc = this.toBytes(str)
    let ascLen = asc.length
    let strLen = str.length
    let dif = ascLen - strLen
    let kLen = dif / 2
    let aLen = strLen - kLen
    let sLen = kLen + (aLen / 1.6)
    return (kLen > korLimit || aLen > engLimit || sLen > korLimit)
  },
  imageServer (url, addUrl) {
    if (!url) {
      return addUrl || ''
    }
    // if (location.protocol.indexOf('https:') >= 0) {
    //   return '//image.smartscore.kr/' + url
    // }
    return '//image.smartscore.kr/' + url
  },
  number (n, d) {
    if (n === 0) { return '0' }
    if (!d) { d = 0 }

    let _float = parseFloat(n, 10)
    if (_float) {
      if (d < 1) return Math.floor(_float).toLocaleString()
      return _float.toFixed(d) // 소수점 포함
    }
    return '-'
  }
}
