import Vue from 'vue'
// import moment from 'moment'

Vue.filter('number', (n, d) => {
  if (n === 0) { return '0' }
  if (!d) { d = 0 }

  let _float = parseFloat(n, 10)
  if (_float) {
    if (d < 1) return Math.floor(_float).toLocaleString()
    return _float.toFixed(d) // 소수점 포함
  }
  return '0'
})

Vue.filter('zerofill', (n, c) => {
  return n.zerofill(c)
})

Vue.filter('sliced', (value, from, to) => {
  if (!value) return ''

  if (!to) return value.toString().slice(from)
  else return value.toString().slice(from, to)
})
