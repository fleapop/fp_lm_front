import $ from 'jquery'
import format from '../utils/format'

let osType = ''
if (navigator.userAgent.match(/Windows Phone 8.1/i) != null) {
  osType = 'wp8'
} else if (navigator.userAgent.match(/Android/i) != null) {
  osType = 'android'
} else if (navigator.userAgent.match(/iPad/i) != null || navigator.userAgent.match(/iPhone/i) != null || navigator.userAgent.match(/iPod/i) != null) {
  osType = 'ios'
} else {
  osType = 'unknown'
}

let showLoginKey = ''
// const isHybrid = (osType === 'ios' && typeof (window.webkit) === 'object') || (osType === 'android' && typeof (window.bridgeHandler) === 'object')
const isHybrid = (osType === 'ios' ) || (osType === 'android' )
const callbacks = (() => {
  let queue = {}
  let generateUid = () => {
    function S4 () {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
    }

    return (S4() + S4() + S4() + S4() + S4())
  }
  let regist = (callback) => {
    var _hash = generateUid()
    while (queue.hasOwnProperty(_hash)) {
      _hash = generateUid()
    }
    queue[_hash] = callback
    return _hash
  }
  let get = (key) => {
    if (queue.hasOwnProperty(key)) {
      return queue[key]
    }
    return false
  }
  let del = (key) => {
    if (get(key) !== false) {
      delete queue[key]
    }
  }

  return {
    regist (callback) {
      return regist(callback)
    },
    get (key) {
      return get(key)
    },
    del (key) {
      del(key)
    }
  }
}).call({})

const functions = (() => {
  let queue = {}
  let regist = (key, func) => {
    if (queue.hasOwnProperty(key)) {
      delete queue[key]
    }
    queue[key] = func
    return key
  }
  let get = (key) => {
    if (queue.hasOwnProperty(key)) {
      return queue[key]
    }
    return false
  }
  let del = (key) => {
    if (get(key) !== false) {
      delete queue[key]
    }
  }

  return {
    regist (key, func) {
      return regist(key, func)
    },
    get (key) {
      return get(key)
    },
    del (key) {
      if (key !== undefined) {
        del(key)
        return
      }

      for (let qkey in queue) {
        if (queue.hasOwnProperty(qkey)) {
          del(qkey)
        }
      }
    }
  }
}).call({})

const ComponentHistory = (() => {
  let stack = []
  let curPos = -1
  let curPath = null
  let repeatMode = false
  let forwardPath = null
  let lastKey = null
  let push = (obj, route) => {
    if (typeof (obj) !== 'object') {
      console.warn('### push - argument is not object')
      return false
    }
    ++curPos
    let uid = curPos
    stack[uid] = obj
    obj._uid = uid
    obj._parentPath = route.fullPath
    obj._parentName = route.name
    obj._key = window.performance.now().toFixed(3)
    window.history.pushState({uid: uid, path: obj._parentPath, key: obj._key}, '', '')
    curPos = uid
    curPath = route.fullPath
    console.log('### push - uid: ' + uid + ', path:' + curPath + ', obj:', JSON.stringify(obj))
    return uid
  }
  let routed = (to, from) => {
    console.log('### routed - curPath:' + curPath + ', toPath:' + to.fullPath + ', curPos:' + curPos + ', fromPath:' + from.fullPath)
    if (curPos === -1) return
    if (stack[curPos]._parentPath !== to.fullPath) {
      console.log('### routed - route forward')
      for (let i = curPos; i > -1; --i) {
        let o = stack[i]
        console.log(o._parentPath, curPath, o.closeFunc)
        if (o._parentPath === curPath && typeof (o.closeFunc) === 'function') {
          try {
            o.closeFunc.call({})
          } catch (e) {
            console.log(e)
          }
          console.log('### routed - close func', o)
          delete o.closeFunc
        }
      }
      curPath = forwardPath = to.fullPath
    } else {
      forwardPath = null
    }
  }
  let current = () => {
    return curPos
  }
  let length = () => {
    return stack.length
  }
  let processPop = (state, app) => {
    let uid = state && typeof (state.uid) === 'number' ? state.uid : null
    let key = state && state.key ? parseInt(state.key, 10) : null
    let isBack = true
    console.log('### processPop - uid:' + uid + ', key: ' + key + ', curPos:' + curPos + ', curPath:' + curPath + ', forwardPath:' + forwardPath + ', curRoutePath:' + app.$route.fullPath)

    if (lastKey !== null) {
      if (lastKey < key) isBack = false
    }
    lastKey = key

    if (isBack && forwardPath !== app.$route.fullPath) {
      // if ((uid === null || uid <= curPos) && forwardPath !== app.$route.fullPath) {
      // back
      console.log('### processPop - back')
      let bo = stack[curPos]
      if (bo && typeof (bo.closeFunc) === 'function') {
        try {
          bo.closeFunc.call({})
        } catch (e) {
          console.log(e)
        }
        delete bo.closeFunc
        console.log('### processPop - called close function')
      }
      curPos = uid === null ? -1 : uid
      let co = stack[curPos]
      if (co) {
        app.$bus.$emit('component_history_back', co._parentPath, co._uid)
        console.log('### processPop - sent component_history_back', co._parentPath, co._uid)

        if (curPath !== co._parentPath) {
          repeatMode = true
        }
        curPath = co._parentPath
      }

      if (repeatMode && curPos > -1) {
        history.back()
      } else {
        repeatMode = false
      }
    } else {
      // forward
      console.log('### processPop - forward')
      if (typeof (uid) === 'number') {
        curPos = uid
        let co = stack[curPos]
        if (co) {
          app.$bus.$emit('component_history_forward', co._parentPath, co._uid)
        }
      }
    }

    console.log('### end processPop - curPos:' + curPos + ', curPath:' + curPath)
    forwardPath = null
  }
  let getData = (uid) => {
    if (stack[uid]) {
      return stack[uid]
    }
    return null
  }
  let setClose = (uid, func) => {
    if (typeof (uid) === 'number') {
      let o = stack[uid]
      if (o) {
        o.closeFunc = func
        console.log('### setClose - added close function! for uid: ' + uid)
      }
    }
  }
  let removeAtCount = (len) => {
    if (stack.length > 0) {
      for (let i = 0; i < len; i++) {
        let func = stack.pop()
        func.call({}, true)
      }
    }
    return {}
  }
  let removeAll = () => {
    stack.splice(0, stack.length)
  }

  return {
    push,
    routed,
    current,
    length,
    processPop,
    getData,
    setClose,
    removeAtCount,
    removeAll
  }
}).call({})
export {ComponentHistory}

const scrollTopStack = (() => {
  let stack = []
  let push = (el, mode) => {
    if (typeof (el) !== 'object') {
      console.warn('argument is not DOM')
      return false
    } else {
      let nodeName
      if (el.length > 0) {
        nodeName = el[0].nodeName
      } else {
        nodeName = el.nodeName
      }
      if (!nodeName) {
        console.warn('argument is not DOM')
        return false
      }
    }
    if (mode.toLowerCase() === 'replace') {
      stack.pop()
    }
    stack.push(el)
    return true
  }
  let pop = () => {
    if (stack.length > 0) {
      stack.pop()
    }
  }
  let length = () => {
    return stack.length
  }
  let scrollTop = () => {
    /*
    if (stack.length > 0) {
      let el = stack[stack.length - 1]
      if (el.length > 0) {
        el.animate({scrollTop: 0}, 200)
      } else {
        $(el).animate({scrollTop: 0}, 200)
      }
    }
    */
    console.log($('.c-scroll-top').last())
    $('.c-scroll-top').last().animate({scrollTop: 0}, 200)
  }

  return {
    push (el, mode) {
      return push(el, mode)
    },
    pop () {
      pop()
    },
    length () {
      return length()
    },
    scrollTop () {
      scrollTop()
    }
  }
}).call({})

export function callbackFromNative (key, result, del = true) {
  /**
   * Native -> js
   * params: key - callback function regist key
   * params: result - callback function argument
   */
    // result.replace(/\n/gi, '\n')
  let callbackFunction = callbacks.get(key)
  if (callbackFunction !== false) {
    console.info('#toNative Callback Key: ' + key + ', Result: ' + result)
    console.log('from key : ' + showLoginKey + ' , to key : ' + key)
    console.log('result : ', result)
    if (key === showLoginKey && String(result) === 'true') {
      console.log(localStorage.getItem('user_data'))
      let userData = JSON.parse(localStorage.getItem('user_data'))
      Native.closeLauncher(userData.user_idx, userData.last_pb_sync_date)
      showLoginKey = ''
    }
    if (result) {
      if (typeof (result) === 'string') {
        result = JSON.parse(format.stringEnterCharConvert(result))
      }
    }
    callbackFunction.call(this, result)
    if (del) callbacks.del(key)
  } else {
    console.info('#toNative Callback Not Exist Key: ' + key + ', Result: ' + result)
  }
}

export function callFuncFromNative (key, param) {
  /**
   * Native -> js
   * params: key - callback function regist key
   */
  let callFunction = functions.get(key)
  if (callFunction !== false) {
    console.info('#toNative Function: ', key, ', Param: ', param)
    if (typeof (param) === 'string') {
      param = JSON.parse(format.stringEnterCharConvert(param))
    }
    return callFunction.call(this, param)
  } else {
    console.info('#toNative Function Not Exist Key: ', key, ', Param: ', param)
  }
}


let scrollToTop = {
  callback: null,
  push (el) {
    $(el).addClass('c-scroll-top')
  },
  scrollTop () {
    if (this.callback !== null) {
      this.callback()
    } else {
      scrollTopStack.scrollTop()
    }
  },
  addCallback (cb) {
    this.callback = cb
  }
}
export {scrollToTop}

let webFunction = {
  regist (func, key) {
    return functions.regist(key, func)
  },
  clear () {
    functions.del('share')
  }
}
export {webFunction}

const checkNative = (f, p) => {
  if (!isHybrid) {
    console.warn('this browser is not support for native function - func:' + f + ', param:' + p)
    return false
  }
  return true
}

const toNative = (func, param, key) => {
  console.info('#toNative::', func, param, key)
  if (!checkNative(func, param)) {
    return
  }
  param || (param = {})
  key || (key = '')
  // if (typeof (param) === 'object') param = JSON.stringify(param)
  if (typeof (param) === 'string') param = JSON.parse(format.stringEnterCharConvert(param))
  if (func === 'show_login') showLoginKey = key
  console.info('#toNative callNative func: ' + func + ', param: ' + JSON.stringify(param) + ', key: ' + key)
  if (osType === 'ios') {
    // typeof (window.webkit.messageHandlers[func]) === 'object' && window.webkit.messageHandlers[func].postMessage({param: param, key: key})
    window.webkit.messageHandlers.jsToNative.postMessage({func: func, param: JSON.stringify(param), key: key})

  } else {
    window.bridgeHandler.jsToNative(func, JSON.stringify(param), key)
  }
}

const toNativeCallback = (func, param, callback) => {
  if (!checkNative(func, param)) {
    callback()
    return
  }
  let key = callbacks.regist(callback)
  toNative(func, param, key)
}

let Native = {
  getOsType () {
    return osType
  },
  getOsTypeCode () {
    let code
    if (osType === 'android') {
      code = 103
    } else if (osType === 'ios') {
      code = 102
    } else {
      code = 101
    }
    return code
  },
  isHybrid () {
    console.log(isHybrid)
    console.log(window.webkit)
    console.log(osType)

    return isHybrid
  },
  historyBack () {
    if (isHybrid) {
      let param = {}
      toNative('historyBack', param)
    } else {
      history.back()
    }
  },
  windowClose () {
    localStorage.removeItem('stackHistory')
    if (isHybrid) {
      let param = {}
      toNative('windowClose', param)
    } else {
      history.back()
    }
  },
  alert (msg) {
    if (isHybrid) {
      let param = {
        msg: msg
      }
      toNative('alert', param)
    } else {
      alert(msg)
    }
  },
  toast (msg) {
    let param = {
      msg: msg
    }
    toNative('toast', param)
  },
  confirm (msg, callback) {
    let param = {
      msg: msg
    }
    toNativeCallback('confirm', param, callback)
  },
  storeSharePop (title, goodsId) {
    let param = {
      title: title,
      idx: goodsId
    }
    toNative('storeSharePop', param)
  },
  setAddress () {
    let param = {
    }
    toNativeCallback('setAddress', param)
  },
  getOrderDetail (type, orderId) {
    let param = {
      type: type,
      orderId: orderId
    }
    console.log('getOrderDetail')
    toNative('getOrderDetail', param)
  },
  openLoginPop () {
    toNative('openLoginPop')
  },
  goPageOpen (type, idx, is_img = 0) {
    if (isHybrid) {
      let param = {
        type: type,
        idx: idx,
        is_img: is_img
      }
      toNative('goPageOpen', param)
    }
  }
}

export default Native
