import Native from './native'

export const _q = {}

export function jsonToQuery (json) {
  if (json && typeof (json) === 'object') {
    var __q = []

    for (var k in json) {
      if (typeof (json[k]) !== 'undefined' && json[k] !== null) {
        try {
          var _key = decodeURIComponent(k)
        } catch (e) {
          _key = k
        }
        try {
          var _val = decodeURIComponent(json[k])
        } catch (e) {
          _val = json[k]
        }
        __q.push(encodeURIComponent(_key) + '=' + encodeURIComponent(_val))
      }
    }
    return '?' + __q.join('&')
  }
}

export function queryToArray () {
  var _qStr = location.search.replace('?', '')
  var _qArr = _qStr.split('&')
  for (var i in _qArr) {
    var _qSplit = _qArr[i].split('=')
    _q[_qSplit[0]] = _qSplit[1]
  }
}

export function setCookie (key, val, expireHour, expireDays = null, path, domain) {
  var expires = ''
  var date = new Date()
  if (expireHour) {
    date.setTime(date.getTime() + (expireHour * 60 * 60 * 1000))
    expires = '; expires=' + date.toGMTString()
  } else if (expireDays) {
    expires = '; expires=' + expireDays
  }

  var domains = ''
  if (domain) domains = '; domain=' + domain

  var paths = ''
  if (path) paths = '; path=' + path
  document.cookie = key + '=' + val + expires + domains + paths
}
/**
 * @author dhlee0@smartscore.kr
 * @version 0.1
 * @since 2020년 04월 08일
 * @description 쿠키 삭제
 수정 로그
 version 0.1
 */
export function removeCookie (key) {
  document.cookie = `${key}=true; expires=${(new Date(Date.now() - (24 * 60 * 60 * 1000))).toUTCString()};`
}

function isArray (arr) {
  return arr instanceof Array
}

export function getCookie (name) {
  var cookies = document.cookie.split(';')
  if (cookies && isArray(cookies)) {
    var result = cookies.find(function (o) {
      return (o.indexOf(name + '=') > -1)
    })
    if (result) {
      try {
        return result.substr(result.indexOf('=') + 1)
      } catch (e) {
        return null
      }
    } else {
      return null
    }
  } else {
    return null
  }
}

export function getLocalStorageData (key, param) {
  let obj = localStorage.getItem(key)
  obj = JSON.parse(obj)
  return obj ? (param ? obj[param] : obj) : {}
}

export function setLocalStorageData (key, param, val) {
  let obj = localStorage.getItem(key)

  if (!obj) {
    obj = {}
  } else {
    obj = JSON.parse(obj)
  }

  if (val !== undefined) {
    if (typeof (param) === 'object') {
      if (obj[param[0]] === undefined) obj[param[0]] = {}
      obj[param[0]][param[1]] = val
    } else {
      obj[param] = val
    }
  } else {
    obj = param
  }

  localStorage.setItem(key, JSON.stringify(obj))
}

class StopWatch {
  constructor () {
    this.startTime = 0
    this.lastTime = 0
  }

  start (title) {
    this.startTime = this.lastTime = new Date().getTime()
    console.log('[#[StopWatch start] ' + title + ']')
  }

  lap (title, limit) {
    let now = new Date().getTime()
    let dif = (now - this.lastTime) / 1000
    console.log('[#[StopWatch lap] ' + title + ' : ' + dif + (limit && dif > limit ? ' !!OVER LIMIT!! ' + limit : '') + ']')
    this.lastTime = now
  }

  end (title, limit) {
    let now = new Date().getTime()
    let dif = (now - this.lastTime) / 1000
    let elapsed = (now - this.startTime) / 1000
    console.log('[#[StopWatch end] ' + title + ' (dif: ' + dif + ') : (elpased:' + elapsed + ')' + (limit && elapsed > limit ? ' !!OVER LIMIT!! ' + limit : '') + ']')
  }
}

let sw = new StopWatch()

class ScrollHistoryMap {
  constructor () {
    this.map = {}
  }

  add (key) {
    let ele = document.getElementsByClassName('c-scroll-top')
    if (ele.length > 0) {
      let last = ele.length - 1
      let top = ele[last].scrollTop
      let height = ele[last].scrollHeight
      let bodyHeight = document.body.clientHeight
      if (height > bodyHeight) {
        this.map[key] = {scrollTop: top, scrollHeight: height, key: key, fail: 0}
      }
      console.log('------------ scroll add - lastIdx: ' + last + ', top:' + top + ', height:' + height + ', body:' + bodyHeight, key, this.map[key])
    }
  }

  get (key) {
    return this.map[key] ? this.map[key] : null
  }

  del (key) {
    delete this.map[key]
  }

  scroll (obj) {
    console.log('------------ scroll before ------------', obj.scrollTop, obj.fail)
    if (obj.scrollTop !== 0) {
      setTimeout((obj) => {
        let ele = document.getElementsByClassName('c-scroll-top')
        if (ele.length === 0) {
          ++obj.fail
          if (obj.fail <= 40) this.scroll(obj)
          return
        }

        let sb = ele[ele.length - 1]
        let bodyHeight = document.body.clientHeight
        let path = location.href.split('smartscore3')[1]
        console.log('------------ scrolling ------------', sb.scrollHeight, bodyHeight, obj.scrollTop, obj.fail, obj.key, path)
        if (sb && sb.scrollHeight > bodyHeight) {
          if (path === obj.key) {
            sb.scrollTop = obj.scrollTop
            obj.fail = 0
          }
        } else {
          ++obj.fail
          if (obj.fail <= 40) this.scroll(obj)
        }
      }, 200, obj)
    }
  }

  clear () {
    console.log('------------ scroll reset ------------')
    this.map = {}
  }

  show () {
    console.log('------------ scroll history ----------')
    console.log(this.map)
  }
}

let shm = new ScrollHistoryMap()

let Util = {
  enc (str, key = '3dxbWFjbQ') {
    if (typeof (str) !== 'string') str = str.toString()
    let bytes = this.toBytes(str)
    let len = bytes.length
    let keyLen = key.length

    if (!len || !keyLen || keyLen < 3) return ''

    let ret = ''
    for (let i = 0; i < len; i++) {
      let keyCharAt = i % keyLen
      let keyChar = key[keyCharAt]
      let keyCode = keyChar.charCodeAt(0)

      let charCode = bytes[i]
      ret += String.fromCharCode(charCode ^ keyCode)
    }
    return btoa(ret)
  },
  toBytes (str) {
    let out = []
    let p = 0
    for (let i = 0; i < str.length; i++) {
      let c = str.charCodeAt(i)
      if (c < 128) {
        out[p++] = c
      } else if (c < 2048) {
        out[p++] = (c >> 6) | 192
        out[p++] = (c & 63) | 128
      } else if (((c & 0xFC00) === 0xD800) && (i + 1) < str.length && ((str.charCodeAt(i + 1) & 0xFC00) === 0xDC00)) {
        c = 0x10000 + ((c & 0x03FF) << 10) + (str.charCodeAt(++i) & 0x03FF)
        out[p++] = (c >> 18) | 240
        out[p++] = ((c >> 12) & 63) | 128
        out[p++] = ((c >> 6) & 63) | 128
        out[p++] = (c & 63) | 128
      } else {
        out[p++] = (c >> 12) | 224
        out[p++] = ((c >> 6) & 63) | 128
        out[p++] = (c & 63) | 128
      }
    }
    return out
  },
  checkStrLenOver (str, korLimit, engLimit) {
    let asc = Util.toBytes(str)
    let ascLen = asc.length
    let strLen = str.length
    let dif = ascLen - strLen
    let kLen = dif / 2
    let aLen = strLen - kLen
    let sLen = kLen + (aLen / 1.6)
    return (kLen > korLimit || aLen > engLimit || sLen > korLimit)
  },
  setItem (key, value) {
    let lsKey = '_MA3_' + key
    let val = typeof (value) !== 'string' ? JSON.stringify(value) : value
    window.localStorage.setItem(lsKey, val)
  },
  getItem (key) {
    let lsKey = '_MA3_' + key
    return localStorage.getItem(lsKey)
  },
  setSesItem (key, val) {
    sessionStorage.setItem(key, val)
  },
  getSesItem (key) {
    return sessionStorage.getItem(key)
  },

  isValidPhone (phone) {
    let regPhone = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/
    return phone ? phone.match(regPhone) !== null : false
  },
  isValidEmail (email) {
    let regEmail = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i
    return email.match(regEmail) !== null
  },
  getCodeName (ar, keyPropName, val, retPropName) {
    if (!Array.isArray(ar)) return 'na'
    let fo = ar.find(o => {
      return o[keyPropName] + '' === val + ''
    })
    return fo ? fo[retPropName] : ''
  },
  compareV3Version (tv, pv) {
    let isIOS = Native.getOsType() === 'ios'
    // if (!isIOS && pv.substr(0, 1) !== 'R') return 1 // 폰 버젼이 R로 시작하지 않으면 타겟버젼이 높은거임 (ios 제외)
    let tv1 = tv[0] === 'R' ? tv.substr(1) : tv
    let pv1 = pv[0] === 'R' ? pv.substr(1) : pv // R을 떼냄
    console.log('tv: ' + tv1 + ', pv: ' + pv1)
    let ta = tv1.split('.')
    let pa = pv1.split('.')
    let tc = ta.length
    let pc = pa.length
    let len = tc > pc ? tc : pc
    for (let i = 0; i < len; ++i) {
      let t = tc > i ? parseInt(ta[i], 10) : 0
      let p = pc > i ? parseInt(pa[i], 10) : 0
      if (t > p) return 1
      if (p < t) return -1
    }
    return 0
  },
  currentTimestamp () {
    return Math.floor(new Date().getTime() / 1000)
  },
  copyToClipboard (str) {
    const el = document.createElement('textarea')
    el.value = str
    document.body.appendChild(el)
    if (Native.getOsType() === 'ios') {
      el.contentEditable = true
      el.readOnly = false
      let range = document.createRange()
      range.selectNodeContents(el)
      let s = window.getSelection()
      s.removeAllRanges()
      s.addRange(range)
      el.setSelectionRange(0, 999999) // A big number, to cover anything that could be inside the element.
    } else {
      el.setAttribute('readonly', '')
      el.style.position = 'absolute'
      el.style.left = '-9999px'
      el.select()
    }
    let ret = document.execCommand('copy')
    document.body.removeChild(el)

    let ua = (navigator.userAgent).toLowerCase()
    let match = ua.match(/android\s([0-9]*)/)
    let bVer = match ? match[1] : undefined
    if (bVer === '4.4.4' || bVer === '4.4.3' || bVer === '4.4.2') {
      // Native clipboard is used in 4.4.2, 4.4.3
      Native.copyClipBoard(str)
      return !ret
    }
    return ret
  },

  // ios date 이슈로 인해 공통화
  getDateFromString (str) {
    let dt = str.replace('T', ' ').replace(/-/g, '/').replace(/\./g, '/')
    // console.log('from date string: ' + str + ', to: ' + dt)
    return new Date(dt)
  },

  swStart (title) {
    sw.start(title)
  },
  swLap (title, limit = null) {
    sw.lap(title, limit)
  },
  swEnd (title, limit) {
    sw.end(title, limit)
  },

  shmAdd (key) {
    shm.add(key)
  },
  shmGet (key) {
    return shm.get(key)
  },
  shmDel (key) {
    shm.del(key)
  },
  shmClear () {
    shm.clear()
  },
  shmSetScroll (obj) {
    shm.scroll(obj)
  },
  shmShow () {
    shm.show()
  }

}

export default Util
