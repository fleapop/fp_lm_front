// import filters from '../filters'
// import directives from '../directives'
import http from '../apis/http'
export default {
  install (Vue) {
    // console.log('plugins::filters::', filters)
    // Object.keys(filters).map(name => Vue.filter(name, filters[name]))
    // console.log('plugins::directives::', directives)
    // Object.keys(directives).map(name => Vue.directive(name, directives[name]))
    // console.log('plugins::apis::', Apis)
    Vue.prototype.$http = http
    // Vue.prototype.$api = Apis.apis
  }
}
