import Vue from 'vue'
import subviewStackMixin from '@/mixins/subview-stack-mixin'
import Native from "@/lib/native";
// let IMP = window.IMP; // 생략해도 괜찮습니다.
// IMP.init("imp56251768");
export default {
  name: 'IMPpg',
  mixins: [subviewStackMixin],
  created() {

    },
  methods: {
    excutePayment(result) {
      let mdirect_url = 'https://lm.fleapop.co.kr'
      // let mdirect_url = 'http://127.0.0.1:8082'
      // let jsonData = JSON.stringify(result);
      let jsonData = encodeURI(encodeURIComponent(this.CryptoJS.AES.encrypt(JSON.stringify(result), "fleapop21!$&").toString()))
      if (window.location.hostname.indexOf('test') >= 0) {
        mdirect_url = 'https://lmtest.fleapop.co.kr'
        // protocol = 'https://'
      }
      let getNative = Native.getOsType()
      let appScheme = getNative === 'ios' ? 'fp.FleapopProject' : 'iamportapp'

      let pg = 'kcp'
      if (result.method == 'PAYCO') {
        pg = 'kcp.IP02U'
      } else if (result.method == 'kakaopay') {
        pg = 'kakaopay'
      }
      
      let params = {
        pg: pg,
        pay_method: result.method,
        merchant_uid: 'mct_buy_' + result.user_id + '_' + new Date().getTime(),
        name: '러블리마켓',
        amount: result.total_price,
        buyer_email: result.email,
        buyer_name: result.order_name,
        buyer_tel: result.order_phone,
        buyer_addr: result.shipping_address_1 + " " + result.shipping_address_2,
        buyer_postcode: result.shipping_zipcode,
        m_redirect_url: mdirect_url + '/Store/Store/GoodsOrderResult/' + jsonData,
        app_scheme : appScheme
      }

      IMP.request_pay(params, (rsp) => {
        // setPaymentLog(rsp.imp_uid, 'PAY');
        if (rsp.success) {
          this.requestResultSubmit(rsp.imp_uid);
          console.log('################ requst_pay true');
          console.log(rsp);
        } else {
          // this.loadingActivity(false);
          let msg = '결제에 실패하였습니다.';
          msg += rsp.error_msg;
          console.log('################ requst_pay false');
          console.log(rsp);
        }
      });
    },
    requestResultSubmit(imp_uid) {
      let url = "http://lmapi.fleapop.co.kr/api/order/checkout/submit";
      this.orderData.imp_uid = imp_uid;

      let jsonData = JSON.stringify(this.orderData);
      console.log(jsonData);
      let _this = this
      $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        accepts: "application/json",
        url: url,
        data: jsonData,
        success: function (result) {
          // loadingActivity(false);
          // location.href = "/re/store/order/result/" + result;
          _this.stackPush('GoodsOrderResult', null, result)
        },
        error: function (xhr, status, error) {
          console.log(xhr);
          console.log(status);
          console.log();
          // alert(xhr.responseJSON);
          // loadingActivity(false);
        }
      });
    },
    setPaymentLog(imp_uid, action) {
      var url = "/api/payment/log/set/" + imp_uid + "/" + action;
      console.log("setPaymentLog:" + url);
      var callback = function (result) {

      };
      apiCall(url, null, callback, 'GET');
    }
  }
}