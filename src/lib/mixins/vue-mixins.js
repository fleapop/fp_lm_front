import Vue from 'vue'
import Native from '@/lib/native'
export default {
  name: 'vueMixins',
  data () {
    return {
    }
  },
  created () {
    window.requestNativeLogin = this.nativeLogin
    window.requestNativeLogout = this.nativeLogout
    window.nativeStackRouter = this.nativeStackRouter
    window.testAlert = this.testAlert
    window.deliveryPostRefresh = this.deliveryPostRefresh
    Vue.prototype.$native = Native
    Vue.prototype.$querystring = this.queryString
  },
  methods: {
    testAlert (msg) {
      alert(msg)
      // window.requestNativeLogin(112008, '5f4448560186fd2a31e648feb47ac251e5e932d794e8f8535fb8bc4953b60354', 1)
    },
    nativeStackRouter (page, id) {
      this.stackSubPush(page, id)
    },
    queryString (paramName) {
      let tempUrl = window.location.search.substring(1)
      let tempArray = tempUrl.split('&')
      for (let i = 0; i < tempArray.length; i++) {
        let keyValuePair = tempArray[i].split('=')

        if (keyValuePair[0] === paramName) {
          return keyValuePair[1]
        }
      }
    },
    nativeLogin (userIdx, token, account_type) {
      let data = {
        'user_id': userIdx,
        'token': token,
        'device': Native.getOsTypeCode(),
        'account_type': account_type
      }
      this.$http.post('user/native/login', data).then(res => {
        console.log('################## nativeLogin #####################')
        console.log(data)
        console.log('################## nativeLogin #####################')
        let info = res.data.data.info
        info.token = token
        this.setUserInfo(info)
      })
    },
    nativeLogout () {
      let userdata = {
        user_idx: ''
      }
      this.$store.dispatch('setUserData', userdata)
      localStorage.removeItem('user_data')
    },
    setUserInfo (data) {
      console.log(data)
      console.log('#######################################')
      this.$store.dispatch('setUserData', data)
    },
    deliveryPostRefresh (idx) {
      this.$http.get('user/address/' + idx).then(res => {
        this.$bus.$emit('deliveryPostRefresh', res)
      }).catch(e => {
        console.warn(e)
      })
    }
  }
}