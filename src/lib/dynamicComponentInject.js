import ComponentAppendMixin from './mixins/component-append-mixin'
import UIToast from '../components/popup/UIToast'
import UIAlert from '../components/popup/UIAlert'
import LMAlert from '../components/popup/LMAlert'
import SampleHtml from '@/views/Layout/_Components/SampleHtml'

const injectComponent = (Comp, Props) => {
  const ComponentClass = Plugin.Vue.extend(Comp)
  const instance = new ComponentClass({
    propsData: Object.assign({isPlugin: true}, Props),
    router: Plugin.router,
    store: Plugin.store,
    mixins: [ComponentAppendMixin]
  }).$mount()
  console.log('instance::', instance, instance.$root, instance.$root.$el)
  return instance
}



const Plugin = {
  store: null,
  router: null,
  Vue: null,
  isSingletonLoginPop: false,
  install (Vue, option = {}) {
    if (this.installed) return
    this.installed = true

    this.Vue = Vue
    this.store = option.store
    this.router = option.router

    const toast = toastPlugin()
    const alert = alertPlugin('alert')
    const confirm = alertPlugin('confirm')
    const LayerPop = instanceOpenParamPlugin(SampleHtml)


    Vue.prototype.$toast = toast.methods
    Vue.prototype.$alert = alert.methods
    Vue.prototype.$confirm = confirm.methods
    Vue.prototype.$LayerPop = LayerPop.methods
  }
}

const instanceOpenParamPlugin = () => {
  const methods = (component, options = {}, ...args) => {
    console.log('plugin test')
    const defaultOptions = {}

    const propsData = Object.assign(defaultOptions, options)
    const instance = injectComponent(component, propsData)
    return instance.open(...args)
  }
  return {
    methods
  }
}

const instanceOpenLoginPlugin = (component) => {
  const methods = (payload) => {
    let isSingleton = true
    let isLoginPop = false
    if (!(payload && payload.mode && (payload.mode === 'single' || payload.mode === 'reg'))) isLoginPop = true
    if (isLoginPop) Plugin.store.commit('setPopupStateParam', {isLoginPop: isLoginPop})
    if (Plugin.isSingletonLoginPop && isSingleton) {
      console.log('loginPop is Singleton Mode')
      return Promise.resolve(false)
    }
    if (isSingleton) Plugin.isSingletonLoginPop = true
    const instance = injectComponent(component)
    instance.$destroy = () => {
      if (isSingleton) Plugin.isSingletonLoginPop = false
      if (isLoginPop) Plugin.store.commit('setPopupStateParam', {isLoginPop: false})
      if (isSingleton) {
        instance.$el.remove()
      }
    }
    return instance.open(payload)
  }
  return {
    methods
  }
}

const toastPlugin = () => {
  const methods = (options) => {
    const defaultOptions = {}

    if (typeof options === 'string' || typeof options === 'number') {
      defaultOptions.message = options
      options = {}
    }
    const propsData = Object.assign(defaultOptions, options)
    injectComponent(UIToast, propsData, true)
  }
  return {
    methods
  }
}

const alertPlugin = (type) => {
  const methods = (message, title = '', options = {}) => {
    console.log(typeof message)
    const defaultOptions = {}
    defaultOptions.type = type
    defaultOptions.message = message
    defaultOptions.title = title

    const propsData = Object.assign(defaultOptions, options)
    const instance = injectComponent(LMAlert, propsData)
    return instance.open()
  }
  return {
    methods
  }
}

const lmAlertPlugin = () => {
  const methods = (options) => {
    const defaultOptions = {}

    if (typeof options === 'string' || typeof options === 'number') {
      defaultOptions.message = options
      options = {}
    }
    const propsData = Object.assign(defaultOptions, options)
    injectComponent(LMAlert, propsData, true)
  }
  return {
    methods
  }
}

const htmlPlugin = (type) => {
  const methods = (message, title = '', options = {}) => {
    console.log(typeof message)
    const defaultOptions = {}
    defaultOptions.type = type
    defaultOptions.message = message
    defaultOptions.title = title

    const propsData = Object.assign(defaultOptions, options)
    const instance = injectComponent(UIAlert, propsData)
    return instance.open()
  }
  return {
    methods
  }
}


export default Plugin
